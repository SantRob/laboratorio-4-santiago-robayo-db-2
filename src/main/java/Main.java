import com.mongodb.MongoException;
import com.mongodb.client.MongoDatabase;

public class Main {
    public static void main(String[] args) {
        try {
            ConnectionDB connectionBD = new ConnectionDB();
            MongoDatabase mongoDatabase = connectionBD.connection();

            if (mongoDatabase != null) {
            Pet pet = new Pet("Canela", 22, "Golden");
            Crud.insertPet(mongoDatabase, "pets", pet);
            Crud.getPets(mongoDatabase, "pets");
            Crud.updateAge(mongoDatabase, "pets", "Canela", 2);
            Crud.deletePet(mongoDatabase, "pets", "Canela");
            Crud.getPets(mongoDatabase, "pets");
            } else {
                System.out.println("Error con las operaciones de la base de datos");
            }
        } catch (MongoException e) {
            System.out.println("Error conexión a MongoDB: " + e.getMessage());
        }
    }
}