import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;

public class ConnectionDB {
    public MongoDatabase connection () {
        MongoDatabase database = null;
        String connectionString = "mongodb://localhost:27017";
        MongoClientSettings settings = MongoClientSettings.builder()
                .applyConnectionString(new ConnectionString(connectionString))
                .build();

        MongoClient mongoClient = MongoClients.create(settings);
        try {
            database = mongoClient.getDatabase("veterinaria");
            System.out.println("Conexión a la base de datos exitosa");
        } catch (Exception e) {
            System.out.println("Error conexión a MongoDB: " + e.getMessage());
        }
        return database;
    }
}