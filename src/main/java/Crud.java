import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

public final class Crud {
    public static void insertPet (MongoDatabase db, String collection, Pet pet) {
        MongoCollection<Document> mongoCollection = db.getCollection(collection);
        Document document = new Document("name", pet.getName())
                .append("age", pet.getAge())
                .append("species", pet.getSpecies());
        mongoCollection.insertOne(document);
        System.out.println("Mascota ingresada correctamente.");
    }

    public static void getPets(MongoDatabase db, String collection) {
        MongoCollection<Document> mongoCollection = db.getCollection(collection);
        FindIterable<Document> pets = mongoCollection.find();
        for (Document pet : pets) {
            System.out.println(pet.toJson());
        }
    }

    public static void updateAge (MongoDatabase db, String collection, String name, int newAge) {
        MongoCollection<Document> mongoCollection = db.getCollection(collection);
        mongoCollection.updateOne(new Document("name", name), new Document("$set", new Document("age", newAge)));
        System.out.println("Mascota actualizada con éxito");
    }

    public static void deletePet (MongoDatabase db, String collection, String name) {
        MongoCollection<Document> mongoCollection = db.getCollection(collection);
        mongoCollection.deleteOne(new Document("name", name));
        System.out.println("Mascota eliminada con éxito");
    }
}
